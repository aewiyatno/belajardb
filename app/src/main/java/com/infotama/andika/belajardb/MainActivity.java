package com.infotama.andika.belajardb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView idText;
    EditText productField;
    EditText quantityField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        idText = (TextView) findViewById(R.id.id_text);
        productField = (EditText) findViewById(R.id.field_product);
        quantityField = (EditText) findViewById(R.id.field_quantity);

    }

    public void newProduct(View view) {
        MyDBHandler dbHandler = new MyDBHandler(this, null, null,1);

        int quantity = Integer.parseInt(quantityField.getText().toString());
        Product product = new Product(productField.getText().toString(), quantity);

        dbHandler.addProduct(product);

        productField.setText("");
        quantityField.setText("");
    }

    public void lookupProduct(View view){
        MyDBHandler dbHandler = new MyDBHandler(this, null,null,1);
        Product product = dbHandler.findProduct(productField.getText().toString());

        if(product != null){
            idText.setText(String.valueOf(product.getID()));
            quantityField.setText(String.valueOf(product.getQuantity()));
            Toast.makeText(MainActivity.this, "Data ditemukan", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Data Tidak ditemukan", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteProduct(View view){
        MyDBHandler dbHandler = new MyDBHandler(this,null,null,1);

        boolean result = dbHandler.deleteProduct(productField.getText().toString());

        if(result){
            idText.setText("-");
            productField.setText("");
            quantityField.setText("");
            Toast.makeText(MainActivity.this, "Data Berhasil Hapus", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Data Tidak Berhasil dihapus", Toast.LENGTH_SHORT).show();
        }
    }

    public void openList(View view){
        Intent intent = new Intent(this,ListActivity.class);

        startActivity(intent);
    }
}
