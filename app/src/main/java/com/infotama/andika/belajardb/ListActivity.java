package com.infotama.andika.belajardb;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class ListActivity extends AppCompatActivity {

    SimpleCursorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Bundle extra = getIntent().getExtras();

        ListView listView = (ListView) findViewById(R.id.listView2);
        MyDBHandler handler = new MyDBHandler(this,null,null,1);
        Cursor cursor = handler.fetchAllRecord();
        String[] from = new String[]{handler.COLOMN_PRODUCTNAME,handler.COLOMN_QUANTITY};
        int[] to = new int[]{R.id.product_label,R.id.quantity_label};
        adapter = new SimpleCursorAdapter(this,
                R.layout.database_row,cursor, from,to,0);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(ListActivity.this, "Clicked " + i , Toast.LENGTH_SHORT).show();

            }
        });

    }

}
