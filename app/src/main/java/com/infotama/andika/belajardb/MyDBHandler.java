package com.infotama.andika.belajardb;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Andika on 06/09/2016.
 */
public class MyDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "productDB.db";
    public static final String TABLE_PRODUCTS = "products";

    public static final String COLOMN_ID = "_id";
    public static final String COLOMN_PRODUCTNAME = "productname";
    public static final String COLOMN_QUANTITY = "quantity";

    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCTS +"(" +
                COLOMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLOMN_PRODUCTNAME +
                " TEXT," + COLOMN_QUANTITY + " INTEGER" + ")";
        db.execSQL(CREATE_PRODUCT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXIST " + TABLE_PRODUCTS);
        onCreate(db);
    }


    public void addProduct(Product product){
        ContentValues value = new ContentValues();
        value.put(COLOMN_PRODUCTNAME, product.getProductName());
        value.put(COLOMN_QUANTITY, product.getQuantity());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_PRODUCTS, null, value);

        db.close();
    }

    public Product findProduct(String productname){
        String query = "SELECT  * FROM " + TABLE_PRODUCTS + " WHERE " + COLOMN_PRODUCTNAME + " = \"" +
                productname + "\"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        Product product = new Product();

        if(cursor.moveToFirst()){
            cursor.moveToFirst();
            product.setID(Integer.parseInt(cursor.getString(0)));
            product.setProductName(cursor.getString(1));
            product.setQuantity(Integer.parseInt(cursor.getString(2)));
            cursor.close();
        } else {
            product = null;
        }
        db.close();
        return product;
    }

    public boolean deleteProduct(String productname){
        boolean result = false;

        String query = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + COLOMN_PRODUCTNAME + " = \"" +
                productname + " \"";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query,null);

        Product product = new Product();

        if(cursor.moveToFirst()){
            product.setID(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_PRODUCTS, COLOMN_ID + " = ?",
                    new String[]{String.valueOf(product.getID())
                    });
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }

    public Cursor fetchAllRecord(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_PRODUCTS, new String[]{COLOMN_ID,
                COLOMN_PRODUCTNAME,COLOMN_QUANTITY},null,null,null,null,null);

        return cursor;
    }

}
